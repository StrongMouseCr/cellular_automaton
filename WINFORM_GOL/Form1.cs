﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WINFORM_GOL
{
    public partial class Form1 : Form
    {
        private Graphics graphics;
        private int resolution;
        private bool[,] field;
        private int rows;
        private int columns;
        private bool isStop = true;
        private int speed;
        private Color colorGrid, colorCell;
        SolidBrush brushColorCell;

        public Form1()
        {
            InitializeComponent();
        }

        private void StartGame()
        {
            nudDensity.Enabled = false;
            nudResolution.Enabled = false;
            Random random = new Random();
            for (int x = 0; x < columns; x++)
                for (int y = 0; y < rows; y++)
                    field[x, y] = random.Next((int)nudDensity.Value) == 0;
            isStop = false;
            timer1.Start();
            Text = "Работает";
        }

        private void NextGeneration()
        {
            brushColorCell = new System.Drawing.SolidBrush(colorCell);
            var newField = new bool[columns, rows];
            for (int x = 0; x < columns; x++)
            {
                for (int y = 0; y < rows; y++)
                {
                    var neighboursCount = CountNeighbours(x, y);
                    if (!field[x, y] && neighboursCount >= (int)minimumB.Value && neighboursCount <= (int)maximumB.Value)
                        newField[x, y] = true;
                    else if (field[x, y] && neighboursCount > (int)maximumS.Value || neighboursCount < (int)minimumS.Value)
                        newField[x, y] = false;
                    else
                        newField[x, y] = field[x, y];
                    if (field[x, y])
                        graphics.FillRectangle(brushColorCell, x * resolution, y * resolution, resolution, resolution);
                }
            }
            field = newField;
            pictureBox.Refresh();
        }

        private void StopGame()
        {
            isStop = true;
            timer1.Stop();
            Text = "Отсановлено";
            nudDensity.Enabled = true;
            nudResolution.Enabled = true;
            nudResolution.Value = nudResolution.Minimum;
            resolution = (int)nudResolution.Value;
            rows = pictureBox.Height / resolution;
            columns = pictureBox.Width / resolution;
            field = new bool[columns, rows];
            for (int x = 0; x < columns; x++)
                for (int y = 0; y < rows; y++)
                    field[x, y] = false;
            DrawGrid();
        }

        private int CountNeighbours(int x, int y)
        {
            int count = 0;
            for (int i = -1; i < 2; i++)
            {
                for (int j = -1; j < 2; j++)
                {
                    int column = (x + i + columns) % columns;
                    int row = (y + j + rows) % rows;
                    bool isSelf = column == x && row == y;
                    if (field[column, row] && !isSelf)
                        count++;
                }
            }
            return count;
        }

        private void buttonStart_Click(object sender, EventArgs e)
        {
            StartGame();
        }

        private void buttonStop_Click(object sender, EventArgs e)
        {
            StopGame();
        }

        private void ActionInTick()
        {
            speed = (int)nudSpeed.Value;
            timer1.Interval = 10 * speed;
            resolution = (int)nudResolution.Value;
            rows = pictureBox.Height / resolution;
            columns = pictureBox.Width / resolution;
            DrawGrid();
            NextGeneration();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            ActionInTick();
        }

        private void DrawGrid()
        {
            pictureBox.Image = new Bitmap(pictureBox.Width, pictureBox.Height);
            graphics = Graphics.FromImage(pictureBox.Image);
            Pen pen = new Pen(colorGrid);
            for (int x = 0; x <= columns; ++x)
                graphics.DrawLine(pen, x * resolution, 0, x * resolution, rows * resolution);
            for (int y = 0; y <= rows; ++y)
                graphics.DrawLine(pen, 0, y * resolution, columns * resolution, y * resolution);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            colorCell = Color.Maroon;
            colorGrid = Color.Gray;
            brushColorCell = new System.Drawing.SolidBrush(colorCell);
            resolution = (int)nudResolution.Value;
            rows = pictureBox.Height / resolution;
            columns = pictureBox.Width / resolution;
            DrawGrid();
            field = new bool[columns, rows];
            for (int x = 0; x < columns; x++)
                for (int y = 0; y < rows; y++)
                    field[x, y] = false;
            Text = "Отсановлено";
        }

        private void DrawRectangle(int x, int y)
        {
            graphics.FillRectangle(brushColorCell, x * resolution, y * resolution, resolution, resolution);
            pictureBox.Refresh();
        }

        private void ClearCell(int x, int y)
        {
            Pen pen = new Pen(colorGrid);
            graphics.FillRectangle(Brushes.Black, x * resolution, y * resolution, resolution, resolution);
            if (isStop)
            {
                for (int xl = x; xl <= x + 1; ++xl)
                    graphics.DrawLine(pen, xl * resolution, y * resolution, xl * resolution, (y + 1) * resolution);
                for (int yl = y; yl <= y + 1; ++yl)
                    graphics.DrawLine(pen, x * resolution, yl * resolution, (x + 1) * resolution, yl * resolution);
                pictureBox.Refresh();
            }
        }

        private void Draw(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                var x = e.Location.X / resolution;
                var y = e.Location.Y / resolution;
                var validationMouse = ValidateMousePosition(x, y);
                if (validationMouse)
                    field[x, y] = true;
                if (isStop)
                    DrawRectangle(x, y);
            }
            if (e.Button == MouseButtons.Right)
            {
                var x = e.Location.X / resolution;
                var y = e.Location.Y / resolution;
                var validationMouse = ValidateMousePosition(x, y);
                if (validationMouse)
                {
                    field[x, y] = false;
                    ClearCell(x, y);
                }
            }
        }

        private void pictureBox_MouseMove(object sender, MouseEventArgs e)
        {
            Draw(sender, e);
        }

        private bool ValidateMousePosition(int x, int y) => x >= 0 && y >= 0 && x < columns && y < rows;

        private void buttonContinue_Click(object sender, EventArgs e)
        {
            nudDensity.Enabled = false;
            nudResolution.Enabled = false;
            resolution = (int)nudResolution.Value;
            rows = pictureBox.Height / resolution;
            columns = pictureBox.Width / resolution;
            isStop = false;
            timer1.Start();
            Text = "Работает";
        }

        private void buttonPause_Click(object sender, EventArgs e)
        {
            isStop = true;
            timer1.Stop();
            Text = "Отсановлено";
        }

        private void buttonClear_Click(object sender, EventArgs e)
        {
            resolution = (int)nudResolution.Value;
            rows = pictureBox.Height / resolution;
            columns = pictureBox.Width / resolution;
            DrawGrid();
            field = new bool[columns, rows];
            for (int x = 0; x < columns; x++)
                for (int y = 0; y < rows; y++)
                    field[x, y] = false;
        }

        private void nudResolution_ValueChanged(object sender, EventArgs e)
        {
            resolution = (int)nudResolution.Value;
            rows = pictureBox.Height / resolution;
            columns = pictureBox.Width / resolution;
            DrawGrid();
        }

        private void minimumB_ValueChanged(object sender, EventArgs e)
        {
            if (maximumB.Value < minimumB.Value)
                maximumB.Value = minimumB.Value;
        }

        private void minimumS_ValueChanged(object sender, EventArgs e)
        {
            if (maximumS.Value < minimumS.Value)
                maximumS.Value = minimumS.Value;
        }

        private void buttonColorGrid_Click(object sender, EventArgs e)
        {
            if (colorDialog1.ShowDialog() == DialogResult.Cancel)
                return;
            colorGrid = colorDialog1.Color;
            ActionInTick();
        }

        private void buttonColorCell_Click(object sender, EventArgs e)
        {
            if (colorDialog2.ShowDialog() == DialogResult.Cancel)
                return;
            colorCell = colorDialog2.Color;
            ActionInTick();
        }

        private void buttonExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        //Какая-то бесполезная хрень
        private void splitContainer1_Panel1_Paint(object sender, PaintEventArgs e)
        {
        }
    }
}